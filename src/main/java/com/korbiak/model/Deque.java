package com.korbiak.model;

import java.util.ArrayList;
import java.util.List;

public class Deque<E> {
    List<E> list;

    public Deque() {
        this.list = new ArrayList<>();
    }

    public void addFirst(E e) {
        E elem = e;
        list.add(e);
        for (int i = 0; i < list.size() - 1; i++) {
            list.set(list.size() - 1 - i, list.get(list.size() - 2 - i));
        }
        list.set(0, e);
    }

    public E removeFirst() {
        E re = list.get(0);
        list.remove(0);
        return re;
    }

    public E getFirst() {
        return list.get(0);
    }

    public void addLast(E e) {
        list.add(e);
    }

    public E removeLast() {
        E el = list.get(list.size() - 1);
        list.remove(list.size() - 1);
        return el;
    }

    public E getLast(){
        return list.get(list.size() - 1);
    }
    
}
