package com.korbiak.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PriorityQueue<E> {
    List<E> list;

    public PriorityQueue() {
        this.list = new ArrayList<>();
    }

    public boolean add(E element) {
        list.add(element);
        list = list.stream().sorted().collect(Collectors.toList());
        return true;
    }

    public E poll() {
        E element = list.get(list.size() - 1);
        list.remove(list.size() - 1);
        return element;
    }

    public E peek() {
        E element = list.get(list.size() - 1);
        return element;
    }

    public int size() {
        return list.size();
    }

}
