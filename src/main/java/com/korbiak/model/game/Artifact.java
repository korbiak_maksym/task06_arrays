package com.korbiak.model.game;

public class Artifact extends Mystery {

    public Artifact() {
        strange = getRandomIntegerWithinRange(10, 80);
    }

    public boolean action(Hero hero) {
        hero.setPower(strange);
        return true;
    }

    @Override
    public String toString() {
        return "Artifact{" +
                "strange=" + strange +
                '}';
    }
}
