package com.korbiak.model.game;

public class Hero {
    private int power;

    public Hero() {
        this.power = 25;
    }

    public void getBuff(int buff){
        this.power += buff;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
