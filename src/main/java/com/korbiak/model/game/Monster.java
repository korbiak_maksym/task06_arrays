package com.korbiak.model.game;

public class Monster extends Mystery {
    public Monster() {
        strange = getRandomIntegerWithinRange(5, 100);
    }

    public boolean action(Hero hero) {
        if (strange > hero.getPower()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Monster{" +
                "strange=" + strange +
                '}';
    }
}
