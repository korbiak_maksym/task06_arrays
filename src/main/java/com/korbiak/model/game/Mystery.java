package com.korbiak.model.game;

import java.util.Random;

public abstract class Mystery {
    public int strange;

    public int getRandomIntegerWithinRange(int min, int max) {
        int spread = max - min;
        return new Random().nextInt(spread + 1) + min;
    }

    public abstract boolean action(Hero hero);
    @Override
    public String toString() {
        return "Mystery{" +
                "strange=" + strange +
                '}';
    }

    public int getStrange() {
        return strange;
    }

    public void setStrange(int strange) {
        this.strange = strange;
    }
}
