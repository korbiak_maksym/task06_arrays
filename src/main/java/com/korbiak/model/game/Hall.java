package com.korbiak.model.game;

public class Hall {
    private Hero hero;
    private Mystery[] door;
    private int count;
    public Hall() {
        this.hero = new Hero();
        door = new Mystery[10];
        initDoors();
        count = 0;
    }

    public int getNumberOfDeath(int k) {
        if (!door[k].action(new Hero())) {
            count++;
        }
        k++;
        if (k != 10) {
            getNumberOfDeath(k);
        }
        return count;
    }

    public String getInfo() {
        String answer = "";
        answer += "Strange of hero: " + hero.getPower() + "\n";
        for (int i = 0; i < 10; i++) {
            answer += i + 1 + "- door: " + door[i].toString() + "\n";
        }
        return answer;
    }

    public String getWay() {
        String answer = "";
        for (int i = 0; i < 10; i++) {
            if (door[i].action(hero)) {
                answer += (i + 1) + "- door ;";
            }
        }
        if (answer.length() == 0) {
            answer += "dead";
        }
        return answer;
    }

    private void initDoors() {
        int rand;
        for (int i = 0; i < 10; i++) {
            rand = (int) Math.round(Math.random());
            if (rand == 0) {
                door[i] = new Monster();
            } else {
                door[i] = new Artifact();
            }
        }
    }
}
