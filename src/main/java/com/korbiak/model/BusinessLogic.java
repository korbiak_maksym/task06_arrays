package com.korbiak.model;

import com.korbiak.model.game.Hall;

public class BusinessLogic {
    Hall gameHall;

    public BusinessLogic() {
        this.gameHall = new Hall();
    }

    //----------------game------------------
    public String getInfo(){
        return gameHall.getInfo();
    }

    public int getNumberOfDeath() {
        return gameHall.getNumberOfDeath(0);
    }

    public String getWay() {
        return gameHall.getWay();
    }
    //------------------arrays----------------
    public String getExAA(int[] arr1, int[] arr2){
        String answer = "";
        int[] arrAnswer = Array.getExAA(arr1, arr2);
        for (int i = 0; i<arrAnswer.length; i++){
            answer += arrAnswer[i] + " ";
        }
        return answer;
    }
    public String getExAB(int[] arr1, int[] arr2){
        String answer = "";
        int[] arrAnswer = Array.getExAB(arr1, arr2);
        for (int i = 0; i<arrAnswer.length; i++){
            answer += arrAnswer[i] + " ";
        }
        return answer;
    }
    public String getExB(int[] arr){
        String answer = "";
        int[] arrAnswer = Array.getExB(arr);
        for (int i = 0; i<arrAnswer.length; i++){
            answer += arrAnswer[i] + " ";
        }
        return answer;
    }
}
