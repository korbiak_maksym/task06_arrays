package com.korbiak.model;

public class Array {

    static int[] getExAA(int[] arr1, int[] arr2) {
        int count = 0;
        for (int value : arr1) {
            for (int item : arr2) {
                if (value == item) {
                    count++;
                    break;
                }
            }
        }
        int index = 0;
        int[] answer = new int[count];
        for (int value : arr1) {
            for (int item : arr2) {
                if (value == item) {
                    answer[index] = value;
                    index++;
                    break;
                }
            }
        }
        return answer;
    }

    static int[] getExAB(int[] arr1, int[] arr2) {
        int count = 0;
        for (int element : arr1) {
            count++;
            for (int value : arr2) {
                if (element == value) {
                    count--;
                    break;
                }
            }
        }


        int index = 0;
        boolean flag = true;
        int[] answer = new int[count];
        for (int value : arr1) {
            for (int item : arr2) {
                if (value == item) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                answer[index] = value;
                index++;
            }
            flag = true;
        }
        return answer;
    }

    static int[] getExB(int[] arr) {
        int flag = 0;
        int count = 0;
        //2, 5, 7, 8, 8, 8, 4
        for (int i = 0; i < arr.length; i++) {
            for (int k = 0; k < arr.length; k++) {
                if (arr[i] == arr[k]) {
                    flag++;
                }
            }
            if (flag < 3) {
                count++;
            }
            flag = 0;
        }
        int[] answer = new int[count];
        int index = 0;
        //2, 5, 7, 8, 8, 8, 4
        for (int i = 0; i < arr.length; i++) {
            for (int k = 0; k < arr.length; k++) {
                if (arr[i] == arr[k]) {
                    flag++;
                }
            }
            if (flag < 3) {
                answer[index] = arr[i];
                index++;
            }
            flag = 0;
        }
        arr = answer;
        return arr;
    }
}
