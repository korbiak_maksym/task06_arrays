package com.korbiak.view;


import com.korbiak.Application;
import com.korbiak.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
    private Controller controller;
    private static Logger logger;

    public View() {
        this.controller = new Controller();
        logger = LogManager.getLogger(Application.class);
    }

    public void show() {
        //playGame();
        //getArray();
    }

    private void getArray() {
        int[] arr1 = {2, 5, 7, 8, 4};
        int[] arr2 = {4, 5, 8, 9, 10};
        logger.trace("getExA(a):" + controller.getExAA(arr1, arr2));
        logger.trace("getExA(b):" + controller.getExAB(arr1, arr2));
        logger.trace("getExB:" + controller.getExB(arr1));
    }

    private void playGame() {
        logger.trace("Info:");
        logger.trace(controller.getInfo());
        logger.trace("Death of doors:" + controller.getNumberOfDeath());
        logger.trace("Way to life: " + controller.getWay());
    }
}
