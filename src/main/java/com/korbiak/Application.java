package com.korbiak;

import com.korbiak.model.Deque;
import com.korbiak.model.PriorityQueue;
import com.korbiak.view.View;

public class Application {

    public static void main(String[] args) {
       new View().show();
    }
}
