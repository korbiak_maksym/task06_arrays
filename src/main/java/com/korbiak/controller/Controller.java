package com.korbiak.controller;

import com.korbiak.model.BusinessLogic;

public class Controller {
    private BusinessLogic businessLogic;

    public Controller() {
        this.businessLogic = new BusinessLogic();
    }

    //----------------game------------------
    public String getInfo() {
        return businessLogic.getInfo();
    }

    public int getNumberOfDeath() {
        return businessLogic.getNumberOfDeath();
    }

    public String getWay() {
        return businessLogic.getWay();
    }
    //---------------arrays--------------

    public String getExAA(int[] arr1, int[] arr2) {
        return businessLogic.getExAA(arr1, arr2);
    }

    public String getExAB(int[] arr1, int[] arr2) {
        return businessLogic.getExAB(arr1, arr2);
    }

    public String getExB(int[] arr) {
        return businessLogic.getExB(arr);
    }
}
